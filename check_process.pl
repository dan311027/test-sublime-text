use Proc::Find qw(find_proc proc_exists);
use strict;
use warnings;

my @args = ("ps -ef|grep sshd");
system(@args) == 0
    or die "system @args failed: $?";
