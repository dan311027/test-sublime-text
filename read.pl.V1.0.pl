#!/usr/db/bin/perl -w 
#version V.1.0
#Creator Robert ROTTER
#

#-------------------------USE---------------------------------------------------#
#package Execute;
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use feature qw(say);
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;
use File::Compare qw(compare);
use ConfigFile;

#use Configure;
#use Execute;
use Text::Diff;
use File::Copy;
use POSIX qw(strftime);

#-------------------------START--------------------------------------------------#
#DELETE UNVANTED FILE FROM WORKING DIRECTORY

my $file = "/home/xtrnrps/perl_test/test_income.txt";
unlink $file;

if ( -e $file ) {
    print "File still exists!\n";
}
else {
    print "File gone.\n";
}

#------QUERRY START AND CREATE COPY IN TMP ---#
#RETRIEVE THE HOSTS_ AND WRITE TO TEMP FILE

my $src1   = '/home/xtrnrps/perl_test/test_income.txt';
my $des1   = '/home/xtrnrps/perl_test/test_in_place.txt';
my $query  = Query->new( 'tcpd_perm_by_hostid', '1.0' );
my $result = $query->query( { hostid => 'dbkpbecm22.rze.de.db.com' } );

open( DATA, ">$src1" );

print DATA"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically at $(date +'%Y-%m-%d %H:%M')
#
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<DATA>) {
    print DATA $_;

}

foreach my $rec ( $result->records ) {
    my $rec        = Record->new($rec);
    my @attributes = $rec->getattrs();          # get list of attributes
    my $hostid     = $rec->getval('hostid');    # get value
    my $daemon     = $rec->getval('service');
    my $client     = $rec->getval('client');

    if ( open( my $FH, ">>", "$src1" ) || die("error: $!") ) {
        print $FH $rec->getval('service') . ":\t"
          . $rec->getval('client') . "\n";
        close($FH);
    }
}
print "\nNew Source File Created\n";


#--------------DIFF-----------#
#CHECK DIFF AND WRITE TO DIFF-FILE

my $diff = '/home/xtrnrps/perl_test/diff_previous.txt';
my $rc   = system("diff -u  $src1 $des1 >> $diff");

if ( compare( "$src1", "$des1" ) == 0 ) {
    print "They're equal\n";
    die;
}

#-----------COPY ORIGINAL AND MAKE BACKUP with DATE---#
my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
my $des2     = '/home/xtrnrps/perl_test/backup_copy_of_des1.txt';

if ( system("cat $des1 > $des2$date_tag") == 0 ) {
    print "Backup file success\n";
}

#-----------CREATE HOST_ALLOW IN /TMP ---------#
# Opening file in append mode using >> #

my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

#---------- Open file to read----------#
open( DATA1, "<$src1" );

# --------Open new file to write and add Template Header-------#
open( DATA2, ">$dest_tmp_file" );

print DATA2"
#############################################
#
# /etc/hosts.allow  -  TCP Wrapper config file
#
# Created automatically at $(date +'%Y-%m-%d %H:%M')
# 
# Do not modify: Changes will be lost !!!
#
ALL:    localhost\n";

while (<DATA1>) {
    print DATA2 $_;
}
print "\nAppending Headder and New Content  Successful!!!\n";

close(DATA1);
close(DATA2);

#------REPLACE WORKING FILE WITH NEW/TMP FILE--------------#
#my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';
my $final_dest_file = '/home/xtrnrps/perl_test/final_dest_file.txt';

if ( system("cat $dest_tmp_file > $final_dest_file") == 0 ) {
    print "Replacement of ETC_HOSTS.allow SUCCESS !! \n";
}

exit;
