#!/usr/bin/perl -w
# V1.0 Robert Rotter id. xtrnrps
# write file and make a backup of file
# read/write file in scalar context
# file handler need (FH) to read the file need to $line = <FH>; or use while

########################################### STRUCTURE ###################################
# TO DO #
#########
# retrieve the Host_allow file
# make a DIFF if the content is the same do EXIT if not GO DONNE
# deleting hanging file  DONE
# Test RcM avail         not donne
# +1 backup ?            not donne
# rcm-query?             not donne
# modify hosts.allow     DONE > need to add the path from and to

#package Execute;
use strict;
use warnings;
use lib qw( /usr/db/RCM/Classes );
use feature qw(say);
use Architecture;
use Machine;
use Query;
use Record;
use Data::Dumper;
use File::Compare qw(compare);
use ConfigFile;

#use Configure;
#use Execute;
use Text::Diff;
use File::Copy;
use POSIX qw(strftime);

#---------------------------------------------------------------------------#
#----NEW DEL FILE------------------------------#
#DELETE UNVANTED FILE FROM WORKING DIRECTORY

my $file = "/home/xtrnrps/perl_test/test_income.txt";
unlink $file;

if ( -e $file ) {
    print "File still exists!\n";
}
else {
    print "File gone.\n";
}

#------QUERRY START AND CREATE COPY IN TMP ---#
#RETRIEVE THE HOSTS_ AND WRITE TO TEMP FILE

my $src1   = '/home/xtrnrps/perl_test/test_income.txt';
my $des1   = '/home/xtrnrps/perl_test/test_in_place.txt';
my $query  = Query->new( 'tcpd_perm_by_hostid', '1.0' );
my $result = $query->query( { hostid => 'dbkpbecm22.rze.de.db.com' } );

foreach my $rec ( $result->records ) {
    my $rec        = Record->new($rec);
    my @attributes = $rec->getattrs();          # get list of attributes
    my $hostid     = $rec->getval('hostid');    # get value
    my $daemon     = $rec->getval('service');
    my $client     = $rec->getval('client');

    if ( open( my $FH, ">>", "$src1" ) || die("error: $!") ) {
        print $FH $rec->getval('service') . ":\t"
          . $rec->getval('client') . "\n";
        close($FH);
    }
}
print "\nNew Source File Created\n";

#--------------DIFF-----------#
#CHECK DIFF AND WRITE TO DIFF-FILE

my $diff = '/home/xtrnrps/perl_test/diff_previous.txt';
my $rc   = system("diff -u  $src1 $des1 >> $diff");

if ( compare( "$src1", "$des1" ) == 0 ) {
    print "They're equal\n";
    die;
}

#-----------COPY ORIGINAL AND MAKE BACKUP---#
my $date_tag = strftime( "_%d-%m-%Y-%H-%M", localtime );
my $des2     = '/home/xtrnrps/perl_test/backup_copy_of_des1.txt';

if ( system("cat $des1 > $des2$date_tag") == 0 ) {
    print "Backup file success\n";
}

#-----------CREATE HOST_ALLOW IN /TMP ---------#
# Opening file in append mode using >> #

my $dest_tmp_file = '/home/xtrnrps/perl_test/destination_tmp_file.txt';

#---------- Open file to read----------#
open( DATA1, "<$src1" );

# --------Open new file to write-------#
open( DATA2, ">>$dest_tmp_file" );

#-------Copy data from one file to another----#
while (<DATA1>) {
    print DATA2 $_;
}
print "\nAppending to File is Successful!!!\n";

close(DATA1);
close(DATA2);

#------MAKE A COPY OF RUNNING FILE--------------#
#cp /etc/hosts.deny /etc/hosts.deny.$DATE
#cp /etc/hosts.allow /etc/hosts.allow.$DATE
exit;
