#!/bin/ksh
# $Id: update_hosts_allow.sh,v 1.33 2022/01/17 10:48:46 czz15524_(Jaroslav_Stava) Exp $
#
#####################################################################
#
# File:        update_hosts_allow.sh
#
# Author:    Volker Wiegand / Bernd Ernesti
#
# See the end of this file for a history log
#
# No log is written because this script is called regularly.
#
#####################################################################

if [[ -z $CONFIG_DEF ]]
then
    . /usr/db/local/make_environ.sh
fi

[[ -n $FULLDEBUG ]] && FULLDEBUG="set -x"
$FULLDEBUG

nodename=$(hostname)
DATE=$(date +%Y%m%d.%H%M)

# use ERRORFILE to circumvent inability to do exit() from function
ERRORFILE=/var/db/work/update_hosts_allow
#cleanup
rm $ERRORFILE 2>/dev/null
do_query() {
    query="$@"
    query_result=$( $DBGETVAL -rc $DBGETVALRC "$@" )
    RC=$?
    # exit 1 if RCM query failed !!!
    if [[ "$RC" -ne 0 ]]; then
        log_entry ERROR $0 "RCM query '$*' failed. Should abort."
	echo "update_hosts_allow script return code is: 1" >$ERRORFILE
    fi
    echo "$query_result"
}

#####################################################################
#
# Check command line args (this is dual headed ...)
#

typeset write_log=no
typeset sec_sv_id=""
typeset serv_list=""
typeset DIFFONLY=no

while getopts ":lds:" opt
do
    case $opt in
        l)  write_log=yes
            ;;
        s)  serv_list="$OPTARG"
            ;;
        d)  DIFFONLY=yes
            ;;
        *)  log_entry ERROR $0 "usage: $0 [-l] [-s serv-list]"
            exit 1
            ;;
    esac
done
shift $OPTIND-1

sec_sv_id=$( do_query -t db_sw_config_table \
    -q "hostid=$nodename and service=Security and \
    function=tcpd" service_id | tr -d '" ' )
if [[ -f $ERROFILE ]]; then
    log_entry ERROR $0 "RCM query failed. Exiting."
    exit 1
fi

if [[ -n $sec_sv_id ]]
then
    . /applications/security/scripts/make_environ_sec.sh

    allowfile="$DB_SEC_TMP/allow.tmp.$$"
    denyfile="$DB_SEC_TMP/deny.tmp.$$"
    grp_table=rcm.sec_tcpd_servergroup
    srv_table=rcm.sec_tcpd_server
else
    allowfile="/tmp/allow.tmp.$$"
    denyfile="/tmp/deny.tmp.$$"
    grp_table=db_tcpd_servergroup_table
    srv_table=db_tcpd_server_table
fi

[[ -n $serv_list ]] && cat /dev/null >$serv_list

#####################################################################
#
# Function to retrieve daemon name from service entry
#
# Caution: there are Tabs in the search strings !!!
#

function get_daemon
{
    typeset serv sock prot mode user path prog rest

    [[ -z $1 ]] && return 0
    
    # -> mk: added to allow for ALL:ALL configurations
    if [[ $1 = "ALL" ]]
    then
        print "ALL"
        return 0
    fi
    # <- mk: added to allow for ALL:ALL configurations
    
    # -> mk: added to allow for syslog-ng configurations
    if [[ $1 = "syslog-ng" ]]
    then
        print "syslog-ng"
        return 0
    fi
    # <- mk: added to allow for syslog-ng configurations

    if [[ $1 = "ssh" ]]
    then
        print "sshd"
        return 0
    fi

    # -> mk: added to allow for pki smb stunnel configurations
    if [[ $1 = "smbs" ]]
    then
        print "smbs"
        return 0
    fi
    # <- mk: added to allow for pki smb stunnel configurations
	
    if [[ $1 = "ldap" ]]
    then
        print "slapd"
        return 0
    fi

    if [[ ${1#sshdfwd-} != $1 ]] || [[ ${1#sendmail} != $1 ]]
    then
        print -- "$1"
        return 0
    fi
	# - added to support nfs mounts on Linux
    if [[ $1 = "portmap" ]]
    then
        print "portmap"
        return 0
    fi

    if [[ $1 = "mountd" ]]
    then
        print "mountd"
        return 0
    fi

    if [[ $1 = "vsftpd" ]]
    then
        print "vsftpd"
        return 0
    fi


    ###
        ### Next line changed to support RPC and internal services - an
    ###
    egrep "^#*$1(/?[0-9]*)[	 ]" /etc/inetd.conf | \
    while read serv sock prot mode user path prog rest
    do
        if [[ ${path##*/} = "tcpd" ]]
        then
            path=$prog
            prog="${prog##*/}"
        fi
        if [[ -n $prog ]]
        then
            print -- "${prog##*/}"
        else
            print -- "${path##*/}"
        fi
        return 0
    done

    ## check for daemon in xinetd
    if grep -q "server *=.*/$1" /etc/xinetd.d/*; then 
        print -- "${1##*/}"
        return 0;
    fi

    ## check for daemon in /etc/services    
    #if grep -q "^$1" /etc/services; then
    ## accomodate rpcbind in "sunrpc          111/tcp         portmapper rpcbind      # RPC 4.0 portmapper TCP"
    if egrep -q "^[^#]*\b$1\b" /etc/services; then
        print -- "${1##*/}"
        return 0;
    fi

    return 1
}


#####################################################################
#
# Read configuration and fill temporary files
#

cat <<-EOFD >$denyfile
	##############################################
	#
	# /etc/hosts.deny  -  TCP Wrapper config file
	#
	# Created automatically at $(date +'%Y-%m-%d %H:%M')
	#
	# Do not modify: Changes will be lost !!!
	#
	ALL:ALL
EOFD


cat <<-EOFA >$allowfile
	##############################################
	#
	# /etc/hosts.allow  -  TCP Wrapper config file
	#
	# Created automatically at $(date +'%Y-%m-%d %H:%M')
	#
	# Do not modify: Changes will be lost !!!
	#
	ALL:    localhost
EOFA


do_query -t $grp_table -q "hostid=$nodename" \
    servergroup | tr -d '"' | while read srvgrp
do
    do_query -t $srv_table -q "servergroup=$srvgrp" \
        service client allow valid_from valid_to \
    | while read svcnam clinam allstr valbeg valend
    do
        svcnam=$(print -- "$svcnam" | tr -d '"')
        clinam=$(print -- "$clinam" | tr -d '"')
        allstr=$(print -- "$allstr" | tr -d '"' | tr 'YN' 'yn')
        valbeg=$(print -- "$valbeg" | tr -d '"')
        valend=$(print -- "$valend" | tr -d '"')

        if [[ $write_log = yes ]]
        then
            log_entry LOG $0 \
                "Looking at $svcnam for $clinam"
        fi

        ###
        ### If the allow flag is not "y", forget it
        ###
        [[ $allstr != "y" ]] && continue

        ###
        ### See if a full start and/or end point is given
        ###
        curr_date="$(date +'%Y-%m-%d %H:%M')"
        full_date=""
        if print -- "$valbeg" | grep -q -e "-"
        then
            full_date="y"
            [[ $curr_date < $valbeg ]] && continue
        fi
        if print -- "$valend" | grep -q -e "-"
        then
            full_date="y"
            [[ $curr_date > $valend ]] && continue
        fi

        ###
        ### If no full date, is there a daily time window?
        ###
        curr_time="$(date +'%H:%M')"
        if [[ -z $full_date && -n $valbeg && -n $valend ]]
        then
            [[ $curr_time < $valbeg ]] && continue
            [[ $curr_time > $valend ]] && continue
        fi

        ###
        ### Last check: is there a daemon?
        ###
        daemon=$(get_daemon $svcnam)
        if [[ -z $daemon ]]
        then
            if [[ $write_log = yes ]]
            then
                log_entry WARNING $0 \
                    "no daemon found for $svcnam"
            else
                print -u2 "no daemon found for $svcnam"
            fi
            continue
        fi

        ###
        ### If we survived until here, use it
        ###
                if [[ $daemon != internal ]]
                then
            print -- "$daemon:\t$clinam" >>$allowfile.ent
                fi

        if [[ -n $serv_list && $svcnam != "ssh" ]]
        then
            print -- $svcnam >>$serv_list
        fi

        if [[ $write_log = yes ]]
        then
            log_entry LOG $0 \
                "Allow $svcnam ($daemon) for $clinam"
        else
            print "Allow $svcnam ($daemon) for $clinam"
        fi
    done
done

# delayed exit due to do_query() fail
if [[ -f $ERRORFILE ]]; then
    log_entry ERROR $0 "RCM query failed. Exiting."
    exit 1
fi
# cleanup
rm $ERRORFILE 2>/dev/null

###
### added to remove duplicate entries in service list file - an
###
if [[ -f $serv_list ]]
then
    sort -u $serv_list >$serv_list.$$
    mv $serv_list.$$ $serv_list
fi

###
### Check if SSH is configured, if not allow ALL 
###
### There is one space and one tabulator in search pattern !!!
###
entry=$(egrep "sshd:" $allowfile.ent)

if [[ -z $entry ]]
then 
   print "There is no ssh configuration for $nodename in RCM !"
   print "SSH will be opened for ALL !!"
   print "sshd:\tALL" >$allowfile.ssh
   cat $allowfile.ent >>$allowfile.ssh
   mv $allowfile.ssh $allowfile.ent 
fi

###
### The entries are complete now, time to sort them
###
sort -u $allowfile.ent >>$allowfile
if [[ $? -ne 0 ]]
then
    if [[ $write_log = yes ]]
    then
        log_entry ERROR $0 "Unable to create $allowfile"
    else
        print -u2 "Unable to create $allowfile"
    fi
    rm -f $denyfile $allowfile $allowfile.ent
    exit 1
fi

rm -f $allowfile.ent 


#####################################################################
#
# Install the files (only if they really have changed)
#

grep -v "^#" /etc/hosts.allow >$allowfile.old 2>/dev/null
grep -v "^#" $allowfile       >$allowfile.new 2>/dev/null
diff $allowfile.old $allowfile.new >/dev/null 2>&1
RC=$?

# support for comparisom only
if [[ "$DIFFONLY" = "yes" ]]
then
   diff -u $allowfile.old $allowfile.new 
   exit $?
fi

if [[ $RC -eq 0 ]]
then
    if [[ $write_log = yes ]]
    then
        log_entry LOG $0 \
            "No need to create new /etc/hosts.allow"
    fi
    print -u2 "No need to create new /etc/hosts.allow"
    rm -f $allowfile $denyfile $allowfile.*
    exit 0
fi

cp /etc/hosts.deny /etc/hosts.deny.$DATE
cp /etc/hosts.allow /etc/hosts.allow.$DATE

mv -f $allowfile /etc/hosts.allow
if [[ $? -ne 0 ]]
then
    if [[ $write_log = yes ]]
    then
        log_entry ERROR $0 "Unable to create /etc/hosts.allow"
    else
        print -u2 "Unable to create /etc/hosts.allow"
    fi
    rm -f $denyfile $allowfile
    exit 1
fi

mv -f $denyfile /etc/hosts.deny
if [[ $? -ne 0 ]]
then
    if [[ $write_log = yes ]]
    then
        log_entry ERROR $0 "Unable to create /etc/hosts.deny"
    else
        print -u2 "Unable to create /etc/hosts.deny"
    fi
    rm -f $denyfile $allowfile
    exit 1
fi

chmod 0444 /etc/hosts.allow /etc/hosts.deny
rm $allowfile.* 
find /etc -mtime +30 -name 'hosts.deny.20*' -exec rm '{}' \;
find /etc -mtime +30 -name 'hosts.allow.20*' -exec rm '{}' \;


#####################################################################
#
# That's it folks. Good night.
#

exit 0

#####################################################################
# $Log: update_hosts_allow.sh,v $
# Revision 1.33  2022/01/17 10:48:46  czz15524_(Jaroslav_Stava)
# another attempt at do_query() via ERRORFILE
# (pipes are implemented as subshells and thus can't propagate global variables)
#
# Revision 1.32  2021/12/15 08:27:01  czz15524_(Jaroslav_Stava)
# second try at do_query() and RCMERROR global variable
# add logging to /var/db/work/update_hosts_allow for monitoring
#
# Revision 1.31  2021/11/12 09:42:23  czz15524_(Jaroslav_Stava)
# * ksh is hell *
# set -o pipefail doesn't work on aix
# moving to original implementation
#
# Revision 1.30  2021/11/11 14:16:40  czz15524_(Jaroslav_Stava)
# ksh is stupid - exit() from function doesn't exit script - reverting do_query()
#
# Revision 1.29  2021/11/11 11:15:40  czz15524_(Jaroslav_Stava)
# - refactor RCM query check into do_query() function
#
# Revision 1.28  2021/11/11 07:38:33  czz15524_(Jaroslav_Stava)
# - add checking of RCM query return code - prevent empty file generation on network problems (inc0020880826-in1491716)
# - leave <30 days old backups of /etc/hosts*
#
# Revision 1.27  2018/11/07 13:20:43  Jaroslav_Stava
# add option diffonly (-d) for server vs. RCM comparison purposes
#
# Revision 1.26  2018/05/10 13:03:28  Jaroslav_Stava
# get_daemon() - daemon name in /etc/services doesn't have to be on the beginning of the line
#   added for rpcbind on rhel6
#
# Revision 1.25  2017/11/02 08:33:45  Jaroslav_Stava
# another fix, apparently testing is a necessity
#
# Revision 1.24  2017/11/02 08:04:04  Jaroslav_Stava
# fix previous 2 commits, apparently get_daemon returns value by print :-/
#
# Revision 1.23  2017/11/01 15:09:09  Jaroslav_Stava
# add check for daemon in /etc/xinetd.d/*
#
# Revision 1.22  2017/10/31 13:37:56  Jaroslav_Stava
# get_daemon - added check from /etc/services
# (so the Help for RCM.TCPD_ENTRY doesn't lie anymore)
#
# Revision 1.21  2011/06/08 17:19:29  Frank_Mohr
# added vsftpd
#
# Revision 1.20  2009/05/11 11:06:24  Martin_Kurz
# workaround for strang bug in if clauses, smbs explicitly listed to support
# stunnel (bad hack)
#
# Revision 1.19  2008/02/12 07:21:50  Frank-Christian_Otto
# logic to keep 'sshd: ALL' in case of missing 'sshd' settings corrected
#
# Revision 1.18  2005/12/09 15:47:09  mkurz
# added support for syslog-ng, bad hack :-/
#
# Revision 1.17  2002/08/29 15:57:21  fmohr
# added portmap and mountd handling
#
# Revision 1.16  2002/02/08 14:55:38  bea
# now it should be sure that ssh is open anytime
#
# Revision 1.15  2001/01/12 17:15:53  fmohr
# changed elseF back to else
#
# Revision 1.14  2001/01/12 14:13:10  mkurz
# allow for ALL:ALL configurations in hosts.allow
#
# Revision 1.13  2000/10/16 10:27:47  fmohr
# functions save_var, get_var and del_var moved to linux pubfuncs directory
#
# Revision 1.12  2000/10/13 13:05:57  fmohr
# delete created variable files after use
#
# Revision 1.11  2000/10/13 12:13:54  fmohr
# fixed problem with pdksh on linux
#
# Revision 1.10  2000/03/14 15:52:50  mkurz
# fixed a bug concerning $serv_list. On behalf of Andreas Nemeth
#
# Revision 1.9  2000/03/06 17:00:25  wittler
# Aenderungen von A. Nemeth wg. rpc-daemons fuer Sun-Cluster
#
# Revision 1.8  2000/03/06 16:05:00  an
# support RPCs and internal services
#
# Revision 1.7  1999/03/14 12:37:42  vwiegand
# reduced ERROR to LOG when not rebuilding hosts.allow
#
# Revision 1.6  1999/03/14 12:31:30  vwiegand
# do not replace /etc/hosts.allow if not changing
#
# Revision 1.5  1999/03/10 19:59:33  vwiegand
# new version: get rid of rcmquery heritage, add ldap
#
# Revision 1.4  1999/02/18 18:21:52  vwiegand
# major rework:
# consolidation of scripts for sec + std service
# std services check rhosts for root and opens up
# general cleanup
#
#####################################################################
