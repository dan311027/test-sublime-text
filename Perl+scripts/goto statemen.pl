=pod
#The goto statement in Perl is a jump statement which is sometimes also 
#referred to as unconditional jump statement

Syntax:

goto LABEL

goto EXPRESSION

goto Subroutine_Name

=cut

# Perl program to print numbers
# from 1 to 10 using goto statement

# function to print numbers from 1 to 10
sub printNumbers()
{
	my $n = 1;
label:
	print "$n ";
	$n++;
	if ($n <= 10)
	{
		goto label;
	}
}
	
# Driver Code
printNumbers();

#goto using Expression:
# Perl program to print numbers
# from 1 to 10 using the goto statement

# Defining two strings
# which contain
# label name in parts
$a = "lab";
$b = "el";

# function to print numbers from 1 to 10
sub printNumbers()
{
	my $n = 1;
label:
	print "$n ";
	$n++;
	if ($n <= 10)
	{
		# Passing Expression to label name
		goto $a.$b;
	}
}
	
# Driver Code
printNumbers();

#goto using Subroutine

# Perl program to print numbers
# from 1 to 10 using goto statement

# function to print numbers from 1 to 10
sub label
{
	print "$n ";
	$n++;
	
	if($n <= 10)
	{
		goto &label;
	}
}

# Driver Code
my $n = 1;
label();

