#BEGIN and END block is used when we want to run some part of the code at the beginning and some part of the code at an end.

# Predefined BEGIN block
BEGIN {
    print "In the begin block\n";
}

# Predefined END block
END {
    print "In the end block\n";
}

print "Hello Perl;\n";

=pod
Output:

In the begin block Hello Perl;
In the end block

