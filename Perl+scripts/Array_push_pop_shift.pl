=pod
Function	Description
push	     Inserts values of the list at the end of an array
pop	         Removes the last value of an array
shift	     Shifts all the values of an array on its left
unshift	     Adds the list element to the front of an array
=cut


