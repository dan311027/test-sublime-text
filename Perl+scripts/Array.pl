#array using qw functions
=pod
qw/STRING/
qw (Expression) 
qw /Expression/ 
qw 'Expression' 
qw {Expression}
=cut

# Perl program to demonstrate qw function

# using qw function
@arr1 = qw /This is a Perl Tutorial by GeeksforGeeks/;

# Creates array2 with elements at
# index 2,3,4 in array1
@arr2 = @arr1[ 2, 3, 4 ];

print "Elements of arr1 are:\n";
foreach $ele (@arr1) {
    print "$ele \n";
}

print "Elements of arr2 are:\n";
foreach $ele (@arr2) {
    print "$ele \n";
}

#Accessing Array Elements:
#Accessing Array Elements: For accessing the elements of an array we must prefix “$” sign 
#before the array variable name followed by the index in square brackets.
# Perl program to demonstrate Array's
# creation and accessing the array's elements

# Creation an array fruits
@fruits = ( "apple", "banana", "pineapple", "kiwi" );

# printing the array
print "@fruits\n";

# Prints the array's elements
# one by one using index
print "$fruits[0]\n";
print "$fruits[1]\n";
print "$fruits[2]\n";
print "$fruits[3]\n";

#Sequential Number Arrays: 
#Perl also provides a shortcut to make a sequential array of numbers or letters. 
#It makes out the user’s task easy. Using sequential number arrays users #
#can skip out loops and typing each element when counting to 1000 or letters A to Z etc

# Perl program to demonstrate
# Sequential Number Arrays

# Sequential Number Arrays for
# numbers and letters
@nums    = ( 1 .. 9 );
@letters = ( a .. h );

# Prints array- nums
print "@nums\n";

# Prints array- letters
print "@letters\n";

#Size of an Array: 
#The size of an array(physical size of the array) can be found by evaluating the array in scalar context. 
#The returned value will be the number of elements in the array. 
#An array can be evaluated in scalar context using two ways:

#Implicit Scalar Context $size = @array;
#Explicit scalar context using keyword scalar $size = scalar @array;

# Perl program to demonstrate
# the length of an array

# declaring an array
@arr = ( 11, 22, 33, 44, 55, 66 );

# Storing the length of array
# in variable imp_size
# implicit scalar context
$imp_size = @arr;

# Storing the length of array
# in variable exp_size
# explicit scalar context
$exp_size = scalar @arr;

print "Size of arr(imp_size) $imp_size\n";
print "Size of arr(exp_size) $exp_size";

#Note: In Perl arrays, the size of an array is always equal to (maximum_index + 1) i.e.
#size = maximum_index + 1

# Perl program to find size and
# maximum index of an array

#!/usr/bin/perl

# Array declaration and
# assigning values to it
@arr = ( 10, 17, 19, 20, 25 );

# to find size of array
$size_of_array = @arr;

# to find Maximum index of array
$maximum_index = $#arr;

# displaying result
print "Maximum Index of the Array: $maximum_index\n";
print "The Size of the Array: $size_of_array\n";

#Iterating through an Array: 
#We can iterate in an array using two ways:
#Iterating through the range: We can iterate through the range by finding the size of an array 
#and then running a for loop from 0 to the size – 1 and then accessing the elements of the array.

# array creation
@arr = ( 11, 22, 33, 44, 55 );

# size of array
$len = @arr;

for ( $b = 0 ; $b < $len ; $b = $b + 1 ) {
    print "\@arr[$b] = $arr[$b]\n";
}
=pod
OUTPUT looks like 
@arr[0] = 11 
@arr[1] = 22 
@arr[2] = 33 
@arr[3] = 44 
@arr[4] = 55
=cut



#Iterating through elements(foreach Loop):
=pod
We can iterate through the elements using
  foreach loop
  . Using this we can directly access the elements of the array using a loop
  instead of running a loop over its range
  and then accessing the element
=cut
# through elements(foreach Loop)
# creating array
@l = ( 11, 22, 33, 44, 55 );

# foreach loop
foreach $a (@l) {
    print "$a ";
}


