#for Loop

for ( $count = 1; $count <= 3; $count++ ) {
    print "GeeksForGeeks\n";
}

#foreach Loop

# Array
@data = ( 'GEEKS', 'FOR', 'GEEKS' );

# foreach loop
foreach $word (@data) {
    print $word;
}

#while Loop

# while loop
$count = 3;
while ( $count >= 0 ) {
    $count = $count - 1;
    print "GeeksForGeeks\n";
}

#Infinite While Loop: While loop can execute infinite times which means there is no terminating condition for this loop.

# infinite while loop
# containing condition 1
# which is always true
while (1) {
    print "Infinite While Loop\n";
}

#A do..while loop is almost same as a while loop. The only difference is that do..while loop runs at least one time.

$a = 10;

# do..While loop
do {

    print "$a ";
    $a = $a - 1;
} while ( $a > 0 );

#until loop is the opposite of while loop. It takes a condition in the parenthesis and it only runs until the condition is false

$a = 10;

# until loop
until ( $a < 1 ) {
    print "$a ";
    $a = $a - 1;
}

#nested loop is a loop inside a loop.
# Perl program to illustrate
# nested while Loop

$a = 5;
$b = 0;

# outer while loop
while ($a < 7)
{
$b = 0;
    
# inner while loop
while ( $b <7 )
{
    print "value of a = $a, b = $b\n";
    $b = $b + 1;
}
    
$a = $a + 1;
print "Value of a = $a\n\n";
}


